---
title: "Ce que j'utilise au quotidien sur tel \"intelligent\" et ordinateur"
order: 1
in_menu: true
---
## Sur mon téléphone "intelligent"


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/F-Droid.png">
    </div>
    <div>
      <h2>F-Droid</h2>
      <p>F-Droid est une banque d'applications libres pour Android</p>
      <div>
        <a href="https://framalibre.org/notices/f-droid.html">Vers la notice Framalibre</a>
        <a href="https://f-droid.org/repository/browse/?fdfilter=f-droid&amp;fdid=org.fdroid.fdroid">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Signal.png">
    </div>
    <div>
      <h2>Signal</h2>
      <p>Application de messagerie et téléphonie mobile respectueuse de la vie privée.</p>
      <div>
        <a href="https://framalibre.org/notices/signal.html">Vers la notice Framalibre</a>
        <a href="https://signal.org">Vers le site</a>
      </div>
    </div>
  </article>
  
<article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/FairEmail.png">
    </div>
    <div>
      <h2>FairEmail</h2>
      <p>Client Android de messagerie, minimaliste privilégiant la lecture et la rédaction, orienté sécurité.</p>
      <div>
        <a href="https://framalibre.org/notices/fairemail.html">Vers la notice Framalibre</a>
        <a href="https://email.faircode.eu/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/OpenVPN%20for%20Android.png">
    </div>
    <div>
      <h2>OpenVPN for Android</h2>
      <p>OpenVPN for Android est un client Android pour OpenVPN permettant de paramétrer un serveur OpenVPN.</p>
      <div>
        <a href="https://framalibre.org/notices/openvpn-for-android.html">Vers la notice Framalibre</a>
        <a href="http://ics-openvpn.blinkt.de/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/NewPipe.png">
    </div>
    <div>
      <h2>NewPipe</h2>
      <p>Client multimédia YouTube, PeerTube, SoundCloud &amp; MediaCCC libre pour Android.</p>
      <div>
        <a href="https://framalibre.org/notices/newpipe.html">Vers la notice Framalibre</a>
        <a href="https://newpipe.schabi.org/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/KeepassXC.png">
    </div>
    <div>
      <h2>KeepassXC</h2>
      <p>KeePassXC est un fork communautaire de KeePassX. Ce logiciel dispose de fonctionnalités supplémentaires.</p>
      <div>
        <a href="https://framalibre.org/notices/keepassxc.html">Vers la notice Framalibre</a>
        <a href="https://keepassxc.org/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/VLC.png">
    </div>
    <div>
      <h2>VLC</h2>
      <p>VLC est un lecteur multimédia très populaire.</p>
      <div>
        <a href="https://framalibre.org/notices/vlc.html">Vers la notice Framalibre</a>
        <a href="https://www.videolan.org/vlc/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/C%C3%A9sium.png">
    </div>
    <div>
      <h2>Césium</h2>
      <p>Application web de gestion de compte (en monnaie libre Ğ1) pour le logiciel Duniter.</p>
      <div>
        <a href="https://framalibre.org/notices/c%C3%A9sium.html">Vers la notice Framalibre</a>
        <a href="https://cesium.app/">Vers le site</a>
      </div>
    </div>
  </article> 

## Sur mon ordinateur

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Firefox.png">
    </div>
    <div>
      <h2>Firefox</h2>
      <p>Le navigateur web épris de liberté distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/firefox.html">Vers la notice Framalibre</a>
        <a href="https://www.mozilla.org/fr/firefox/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/uBlock%20Origin.png">
    </div>
    <div>
      <h2>uBlock Origin</h2>
      <p>Une extension vous laissant le choix de bloquer, ou non, les publicités des sites web que vous rencontrez.</p>
      <div>
        <a href="https://framalibre.org/notices/ublock-origin.html">Vers la notice Framalibre</a>
        <a href="https://github.com/gorhill/uBlock">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/KeepassXC.png">
    </div>
    <div>
      <h2>KeepassXC</h2>
      <p>KeePassXC est un fork communautaire de KeePassX. Ce logiciel dispose de fonctionnalités supplémentaires.</p>
      <div>
        <a href="https://framalibre.org/notices/keepassxc.html">Vers la notice Framalibre</a>
        <a href="https://keepassxc.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Shaarli.png">
    </div>
    <div>
      <h2>Shaarli</h2>
      <p>Un logiciel léger pour sauvegarder, trier et partager des adresses web.</p>
      <div>
        <a href="https://framalibre.org/notices/shaarli.html">Vers la notice Framalibre</a>
        <a href="https://github.com/shaarli/Shaarli">Vers le site</a>
      </div>
    </div>
  </article>



  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Thunderbird.png">
    </div>
    <div>
      <h2>Thunderbird</h2>
      <p>Célèbre client de courriel issu du projet Mozilla, distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/thunderbird.html">Vers la notice Framalibre</a>
        <a href="https://www.thunderbird.net/fr/">Vers le site</a>
      </div>
    </div>
  </article>

<article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Jitsi.png">
    </div>
    <div>
      <h2>Jitsi</h2>
      <p>Visio-conférence, messagerie instantanée et plus à essayer sur meet.jit.si.</p>
      <div>
        <a href="https://framalibre.org/notices/jitsi.html">Vers la notice Framalibre</a>
        <a href="https://jitsi.org/">Vers le site</a>
      </div>
    </div>
  </article>

<article class="framalibre-notice">
  <div>
    <img src="https://beta.framalibre.org/images/logo/PeerTube.png">
  </div>
  <div>
    <h2>PeerTube</h2>
    <p>PeerTube est un logiciel décentralisé et fédéré d'hébergement de vidéos.</p>
    <div>
      <a href="https://beta.framalibre.org/notices/peertube.html">Vers la notice Framalibre</a>
      <a href="https://joinpeertube.org/fr/">Vers le site</a>
    </div>
  </div>
</article>



  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Jirafeau.jpeg">
    </div>
    <div>
      <h2>Jirafeau</h2>
      <p>Partagez rapidement et simplement tous vos fichiers.</p>
      <div>
        <a href="https://framalibre.org/notices/jirafeau.html">Vers la notice Framalibre</a>
        <a href="https://gitlab.com/mojo42/Jirafeau">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Lstu.png">
    </div>
    <div>
      <h2>Lstu</h2>
      <p>Un raccourcisseur d'URL simple et léger.</p>
      <div>
        <a href="https://framalibre.org/notices/lstu.html">Vers la notice Framalibre</a>
        <a href="https://lstu.fr">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Zulip.png">
    </div>
    <div>
      <h2>Zulip</h2>
      <p>Une app pour tchater en groupe</p>
      <div>
        <a href="https://framalibre.org/notices/zulip.html">Vers la notice Framalibre</a>
        <a href="https://zulip.org">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Linphone.png">
    </div>
    <div>
      <h2>Linphone</h2>
      <p>Conférences audio et vidéo, compatible téléphonie.</p>
      <div>
        <a href="https://framalibre.org/notices/linphone.html">Vers la notice Framalibre</a>
        <a href="https://www.linphone.org">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/VLC.png">
    </div>
    <div>
      <h2>VLC</h2>
      <p>VLC est un lecteur multimédia très populaire.</p>
      <div>
        <a href="https://framalibre.org/notices/vlc.html">Vers la notice Framalibre</a>
        <a href="https://www.videolan.org/vlc/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/youtube-dl.png">
    </div>
    <div>
      <h2>youtube-dl</h2>
      <p>Télécharger les vidéos de Youtube et de nombreux autres sites</p>
      <div>
        <a href="https://framalibre.org/notices/youtube-dl.html">Vers la notice Framalibre</a>
        <a href="https://ytdl-org.github.io/youtube-dl/">Vers le site</a>
      </div>
    </div>
  </article> 